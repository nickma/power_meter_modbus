from abc import ABCMeta, abstractmethod

from power_meter import PowerMeter


class IMeterConnection(metaclass=ABCMeta):
    def __init__(self, meter):
        if not isinstance(meter, PowerMeter):
            raise Exception('Bad interface')
        self.meter = meter

    @classmethod
    def version(self): return "1.0"

    # connection concerning methods
    @abstractmethod
    def receive_values(self): raise NotImplementedError


class ModbusTCPMeterConnection(IMeterConnection):

    def __init__(self, meter, ip, port, timeout=0.5):
        super().__init__(meter)
        self.ip      = ip
        self.port    = port
        self.timeout = timeout


class DummyMeterConnection(IMeterConnection):

    def __init__(self, meter, name):
        super().__init__(meter)
        self.name = name


    def receive_values(self) -> bool:
        meter = self.meter
        meter.volt = 230.3
        meter.current = 1.0
        meter.true_power = 230.3
        meter.active_energy_import = 1.3
        meter.active_energy_export = 0.0

        return True


# @ref https://stackoverflow.com/questions/681953/python-class-decorator
def meter_connection_decorator(original_class):
    orig_init = original_class.__init__
    # Make copy of original __init__, so we can call it without recursion

    def __init__(self, connection_class, *args, **kws):
        if not issubclass(connection_class, IMeterConnection):
            raise Exception('Bad interface')
        self.conn = connection_class(self)

        orig_init(self, *args, **kws) # Call the original __init__

    original_class.__init__ = __init__ # Set the class' __init__ to the new one
    return original_class


class ConnectedMeter(PowerMeter):

    # Takes a MeterConnection and its arguments
    def __init__(self, connection_class, *args, **kws):
        if not issubclass(connection_class, IMeterConnection):
            raise Exception('Bad interface')
        self.conn = connection_class(self, *args, **kws)
