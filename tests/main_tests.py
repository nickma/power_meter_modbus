#!/usr/bin/env python3
from meter_connection import DummyMeterConnection, ConnectedMeter
from power_meter import *
import unittest


class BasicDummyMeterConnectionTests(unittest.TestCase):

    #@patch.multiple(IMeterConnector, __abstractmethods__=set())
    #def setUpClass(self, cls):

    def test_meterconnection(self):
        meter = PowerMeter()
        m_connector = DummyMeterConnection(meter, 9600)
        self.assertEqual("1.0", m_connector.version())

        m_connector.receive_values()
        self.assertEqual(230.3, meter.volt)

    def test_connectionDecorator(self):
        meter2 = ConnectedMeter(DummyMeterConnection, 9600)
        meter2.conn.receive_values()
        self.assertEqual(230.3, meter2.volt)

    #@classmethod
    #def tearDownClass(cls):



if __name__ == '__main__':
    unittest.main()
