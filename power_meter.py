
class PowerMeter(object):
    volt = 0.0
    current = 0.0
    # active/true power [W]
    true_power = 0.0
    # [kWh]
    active_energy_import = 0.0
    active_energy_export = 0.0